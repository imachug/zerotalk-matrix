import os
import cryptos

seed = os.environ["seed"]
zeroid_privatekey = os.environ["privatekey"]
zeroid_username = "matrixbridge@zeroid.bit"
zeroid_sign = "G7tuCqOKHt2rOjgbudy2K11NUevd+xxe1R5oAsykErpM0BSko2H0tXT3lAYo6glHbMMBCfO4ILwkrGVxqL0KAxk="

zeroid_addr = cryptos.privtoaddr(zeroid_privatekey)