import requests
import websocket
import time
import json
import hashlib
import cryptos
import os
from config import *
from threading import Lock
from collections import defaultdict

file_locks = defaultdict(Lock)


def createWebsocket(site_address):
    text = requests.get(f"http://127.0.0.1:43111/{site_address}", headers={
        "Accept": "text/html"
    }).text
    wrapper_key = text.split("wrapper_key = \"")[1].split("\"")[0]
    ws = websocket.create_connection(f"ws://127.0.0.1:43111/Websocket?wrapper_key={wrapper_key}")
    return ws


def initUser(ws, site_address, sender):
    # Check whether the bot is allowed to modify data/users/content.json
    ws.send(json.dumps({
        "cmd": "fileValidSigners",
        "params": ["data/users/content.json"],
        "id": 1
    }))
    valid_signers = json.loads(ws.recv())["result"]
    if zeroid_addr in valid_signers:
        # Allow all homeservers except those using .bit suffix.
        # In this case, append .hs to the hostname
        username, hostname = sender[1:].rsplit(":", 1)
        if hostname.endswith(".bit"):
            hostname += ".hs"
        # Make sure that this hostname is already allowed, and
        # allow it if it's not
        content_json = f"ZeroNet/data/{site_address}/data/users/content.json"
        with open(content_json) as f:
            user_content = json.loads(f.read())

        cert_signers = user_content["user_contents"]["cert_signers"]
        permission_rules = user_content["user_contents"]["permission_rules"]
        modified = False
        if "matrix/.*" not in permission_rules:
            permission_rules["matrix/.*"] = {"max_size": 1000000000}
            modified = True
        if hostname not in cert_signers:
            cert_signers[hostname] = []
            modified = True
        if zeroid_addr not in cert_signers[hostname]:
            cert_signers[hostname].append(zeroid_addr)
            modified = True
        if modified:
            with open(content_json, "w") as f:
                f.write(json.dumps(user_content, indent=True))
            # Sign & publish
            ws.send(json.dumps({
                "cmd": "sitePublish",
                "params": {
                    "privatekey": zeroid_privatekey,
                    "inner_path": "data/users/content.json"
                },
                "id": 2
            }))
            ws.recv()

        # Create an identity for a user
        auth_type = "matrix"
        name = f"{username}@{hostname}"
        h1 = hashlib.sha512()
        h1.update(name.encode())
        h1.update(zeroid_privatekey.encode())
        h2 = hashlib.sha512()
        h2.update(h1.digest())
        h2.update(zeroid_privatekey.encode())
        privatekey = cryptos.encode_privkey(h2.digest()[:32], "wif")
        addr = cryptos.privtoaddr(privatekey)

        # Make sure there's a valid signature. We're not using
        # ecdsa_sign() function because it's broken in this fork
        challenge = f"{addr}#{auth_type}/{username}"
        v, r, s = cryptos.ecdsa_raw_sign(cryptos.electrum_sig_hash(challenge), zeroid_privatekey)
        sign = cryptos.encode_sig(v, r, s)
        return privatekey, addr, auth_type, name, sign
    else:
        # Prepare comment content
        privatekey = zeroid_privatekey
        addr = zeroid_addr
        auth_type = "web"
        name = zeroid_username
        sign = zeroid_sign
        return privatekey, addr, auth_type, name, sign


def loadUserData(ws, site_address, addr, release=False):
    # Do we have the file downloaded?
    data_json = f"ZeroNet/data/{site_address}/data/users/{addr}/data.json"
    if not os.path.isfile(data_json):
        # Need file
        ws.send(json.dumps({
            "cmd": "fileNeed",
            "params": [
                f"data/users/{addr}/content.json",
                10
            ],
            "id": 2
        }))
        ws.recv()

    # Try reading
    file_locks[data_json].acquire()
    try:
        with open(data_json) as f:
            return json.loads(f.read())
    except OSError:
        # A new user is being created
        return {}
    finally:
        if release:
            file_locks[data_json].release()


def publishUserData(ws, site_address, addr, data, privatekey):
    # Update
    data_json = f"ZeroNet/data/{site_address}/data/users/{addr}/data.json"
    os.makedirs(os.path.dirname(data_json), exist_ok=True)
    with open(data_json, "w") as f:
        f.write(json.dumps(data, indent=True))
    file_locks[data_json].release()

    # Toggle owned
    ws.send(json.dumps({
        "cmd": "siteSetOwned",
        "params": [True],
        "id": 1000000
    }))

    # Sign & publish
    ws.send(json.dumps({
        "cmd": "siteSign",
        "params": {
            "scripted": True,
            "privatekey": privatekey,
            "inner_path": f"data/users/{addr}/content.json",
            "update_changed_files": True
        },
        "id": 3
    }))
    ws.recv()
    ws.send(json.dumps({
        "cmd": "sitePublish",
        "params": {
            "inner_path": f"data/users/{addr}/content.json",
            "sign": False
        },
        "id": 4
    }))
    ws.recv()
    ws.close()


def getZeroTalkUserData(site_address, sender):
    ws = createWebsocket(site_address)
    time.sleep(5)
    _, addr, _, _, _ = initUser(ws, site_address, sender)
    data = loadUserData(ws, site_address, addr, release=True)
    data["_addr"] = addr
    return data


def postZeroTalk(site_address, topic_uri, sender, body, event_id):
    ws = createWebsocket(site_address)
    time.sleep(5)

    privatekey, addr, auth_type, name, sign = initUser(ws, site_address, sender)

    data = loadUserData(ws, site_address, addr)

    if "next_comment_id" not in data:
        data["next_comment_id"] = 1
    if "comment_metadata" not in data:
        data["comment_metadata"] = {}
    if "comment" not in data:
        data["comment"] = {}
    if topic_uri not in data["comment"]:
        data["comment"][topic_uri] = []

    if body is not None:
        # New message / edit
        if auth_type != "matrix":
            body = f"**{sender}**: {body}"

        # Is there such an event already?
        for comment in data["comment"][topic_uri]:
            if data["comment_metadata"].get(str(comment["comment_id"]), {}).get("event_id") == event_id:
                comment["body"] = body
                break
        else:
            data["comment"][topic_uri].append({
                "body": body,
                "comment_id": data["next_comment_id"],
                "added": int(time.time())
            })
            data["comment_metadata"][data["next_comment_id"]] = {
                "event_id": event_id
            }
            data["next_comment_id"] += 1
    else:
        # Remove message
        data["comment"][topic_uri] = [
            comment for comment
            in data["comment"][topic_uri]
            if not (
                data["comment_metadata"].get(str(comment["comment_id"]), {}).get("event_id") == event_id
            )
        ]

    publishUserData(ws, site_address, addr, data, {
        "auth_type": auth_type,
        "user_id": name,
        "sign": sign,
        "privatekey": privatekey
    })


def upvoteZeroTalk(site_address, sender, comment_uri):
    ws = createWebsocket(site_address)
    time.sleep(5)

    privatekey, addr, auth_type, name, sign = initUser(ws, site_address, sender)

    data = loadUserData(ws, site_address, addr)

    if "comment_vote" not in data:
        data["comment_vote"] = {}
    data["comment_vote"][comment_uri] = int(time.time())

    publishUserData(ws, site_address, addr, data, {
        "auth_type": auth_type,
        "user_id": name,
        "sign": sign,
        "privatekey": privatekey
    })